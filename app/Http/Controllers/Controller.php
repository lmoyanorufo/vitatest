<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;

class Controller extends BaseController
{
    use DispatchesJobs;

    /** @var ViewFactory  */
    protected $viewFactory;

    /** @var ValidationFactory  */
    protected $validationFactory;

    public function __construct(ViewFactory $viewFactory, ValidationFactory $validationFactory)
    {
        $this->viewFactory       = $viewFactory;
        $this->validationFactory = $validationFactory;
    }

    protected function defaultResponse(
        Response $response,
        $payload,
        int $responseCode = 200,
        array $metadata = []
    ): Response {
        $content = $this->viewFactory->make(
            'api.default',
            [
                'payload'  => $payload,
                'metadata' => $metadata
            ]
        );

        return $response
            ->setContent($content)
            ->setStatusCode($responseCode)
            ->header('Content-Type', 'application/json');
    }

    /**
     * Validate the given request with the given rules.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $rules
     * @param  array  $messages
     * @param  array  $customAttributes
     * @return array
     */
    public function validate(Request $request, array $rules,
                             array $messages = [], array $customAttributes = [])
    {
        $this->validationFactory
            ->make($request->all(), $rules, $messages, $customAttributes)
            ->validate();

        return $this->extractInputFromRules($request, $rules);
    }

    /**
     * Get the request input based on the given validation rules.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $rules
     * @return array
     */
    protected function extractInputFromRules(Request $request, array $rules)
    {
        return $request->only(collect($rules)->keys()->map(function ($rule) {
            return explode('.', $rule)[0];
        })->unique()->toArray());
    }
}
