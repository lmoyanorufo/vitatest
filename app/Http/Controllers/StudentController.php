<?php

namespace App\Http\Controllers;

use App\Repository\StudentRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;


class StudentController extends Controller
{
    protected $studentRepository;

    public function __construct(
        StudentRepositoryInterface $studentRepository,
        ViewFactory $viewFactory,
        ValidationFactory $validationFactory
    ){
        parent::__construct($viewFactory, $validationFactory);
        $this->studentRepository = $studentRepository;
    }

    public function index(Response $response)
    {
        return $this->defaultResponse($response, $this->studentRepository->all());
    }


    public function create(Request $request, Response $response)
    {
        try {
            $this->validate($request, [
                'name'        => 'required|string|max:255',
                'description' => 'required|string|max:255'
            ]);
        } catch (ValidationException $ex) {
            return $this->defaultResponse($response, $ex->errors(), 400);
        }

        $student = $this
            ->studentRepository
            ->create(
                $request->get('name'), $request->get('description')
            );

        return $this->defaultResponse($response, $student);
    }

    public function show($id, Response $response)
    {
        return $this->defaultResponse($response, $this->studentRepository->get($id));
    }


    public function update(Request $request, Response $response, $id)
    {
        try {
            $this->validate($request, [
                'name' => 'required|string|max:255',
                'description' => 'required|string|max:255'
            ]);
        } catch (ValidationException $ex) {
            return $this->defaultResponse($response, $ex->errors(), 400);
        }

        $course = $this
            ->studentRepository
            ->update($id, $request->get('name'), $request->get('description'));

        return $this->defaultResponse($response, $course);
    }
}
