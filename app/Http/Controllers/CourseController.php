<?php

namespace App\Http\Controllers;

use App\Repository\CourseRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;

class CourseController extends Controller
{
    const MAX_COURSES_CONTROLLER = 2;
    const COURSE_VALIDATOR = [
        'name'        => 'required|string|max:255',
        'description' => 'required|string|max:255'
    ];

    /** @var CourseRepositoryInterface  */
    protected $courseRepository;

    public function __construct(
        CourseRepositoryInterface $courseRepository,
        ViewFactory $viewFactory,
        ValidationFactory $validationFactory
    ) {
        parent::__construct($viewFactory, $validationFactory);
        $this->courseRepository = $courseRepository;
    }

    public function index(Response $response)
    {
        return $this->defaultResponse($response, $this->courseRepository->all());
    }

    public function create(Request $request, Response $response)
    {
        try {
            $this->validate($request, [
                'name'        => 'required|string|max:255',
                'description' => 'required|string|max:255'
            ]);
        } catch (ValidationException $ex) {
            return $this->defaultResponse($response, $ex->errors(), 400);
        }

        $course = $this
            ->courseRepository
            ->create(
                $request->get('name'), $request->get('description')
            );

        return $this->defaultResponse($response, $course);
    }

    public function show($id, Response $response)
    {
        return $this->defaultResponse($response, $this->courseRepository->get($id));
    }

    public function update(Request $request, Response $response, $id)
    {
        try {
            $this->validate($request, [
                'name' => 'required|string|max:255',
                'description' => 'required|string|max:255'
            ]);
        } catch (ValidationException $ex) {
            return $this->defaultResponse($response, $ex->errors(), 400);
        }

        $course = $this
            ->courseRepository
            ->update($id, $request->get('name'), $request->get('description'));

        return $this->defaultResponse($response, $course);
    }

    public function addStudent(Response $response, string $courseId, string $studentId)
    {
        $studentCourses = $this->courseRepository->listByStudent($studentId);
        if (count($studentCourses) == self::MAX_COURSES_CONTROLLER) {
            return $this->defaultResponse($response,
                [
                    'message' => "The student has reached the limit of courses"
                ],
                403
            );
        }

        $this->courseRepository->addStudent($studentId, $courseId);
        $course = $this->courseRepository->get($courseId);

        return $this->defaultResponse($response, $course);
    }
}
