<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $table = 'courses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    public function students()
    {
        return $this->belongsToMany('App\Models\Student')
            ->withTimestamps();
    }
}
