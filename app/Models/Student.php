<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use Uuids;

    public $incrementing = false;

    protected $table = 'students';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    public function courses()
    {
        return $this->belongsToMany('App\Models\Course')
            ->withTimestamps();
    }
}
