<?php

namespace App\Providers;

use App\Repository\CourseRepositoryInterface;
use App\Repository\Eloquent\CourseRepository;
use Illuminate\Support\ServiceProvider;

class CourseRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CourseRepositoryInterface::class, function($app) {
            return new CourseRepository();
        });
    }

    public function provides()
    {
        return [CourseRepositoryInterface::class];
    }
}
