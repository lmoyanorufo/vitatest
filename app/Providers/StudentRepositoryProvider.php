<?php

namespace App\Providers;

use App\Repository\Eloquent\StudentRepository;
use App\Repository\StudentRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class StudentRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(StudentRepositoryInterface::class, function($app) {
            return new StudentRepository();
        });
    }

    public function provides()
    {
        return [StudentRepositoryInterface::class];
    }
}
