<?php

namespace App\Repository;

interface StudentRepositoryInterface
{
    public function create($name, $description);
    public function update($uuid, $name, $description);
    public function all();
    public function get($uuid);
}
