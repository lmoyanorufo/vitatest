<?php

namespace App\Repository;

interface CourseRepositoryInterface
{
    public function create($name, $description);
    public function update($uuid, $name, $description);
    public function all(): array;
    public function get($uuid);
    public function addStudent($uuidStudent, $uuidCourse);
    public function listByStudent($uuidStudent): array;
}
