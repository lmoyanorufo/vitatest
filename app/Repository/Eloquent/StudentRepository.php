<?php

namespace App\Repository\Eloquent;

use App\Models\Student;
use App\Repository\StudentRepositoryInterface;

class StudentRepository implements StudentRepositoryInterface
{
    public function create($name, $description)
    {
        return Student::create([
            'name'        => $name,
            'description' => $description
        ]);
    }

    public function update($uuid, $name, $description)
    {
        $student              = Student::findOrFail($uuid);
        $student->name        = $name;
        $student->description = $description;
        $student->save();

        return $student;
    }

    public function all()
    {
        return Student::query()->get();
    }

    public function get($uuid)
    {
        return Student::findOrFail($uuid);
    }
}