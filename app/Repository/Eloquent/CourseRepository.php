<?php

namespace App\Repository\Eloquent;

use App\Models\Course;
use App\Models\Student;
use App\Repository\CourseRepositoryInterface;

class CourseRepository implements CourseRepositoryInterface
{
    public function create($name, $description)
    {
        return Course::create([
            'name'        => $name,
            'description' => $description
        ]);
    }

    public function update($uuid, $name, $description)
    {
        $student              = Course::findOrFail($uuid);
        $student->name        = $name;
        $student->description = $description;
        $student->save();

        return $student;
    }

    public function all(): array
    {
        return Course::query()->get()->all();

        //return view('api.courses.all');

//        echo print_r($listItems);

//        $return = [];
//        foreach ($listItems as $item) {
//            $return[] = new CourseDto($item->id, $item->name, $item->description);
//        }
//
//        return $return;
    }

    public function listByStudent($uuidStudent): array
    {
        return Student::findOrFail($uuidStudent)->courses()->get()->all();
    }

    public function get($uuid)
    {
        return Course::findOrFail($uuid);
    }

    public function addStudent($uuidStudent, $uuidCourse)
    {
        /** @var Course $course */
        $course = Course::findOrFail($uuidCourse);

        /** @var Student $student */
        $student = Student::findOrFail($uuidStudent);

        // Si este no tiene varios cursos

        $listCourses = $student->courses()->get();

        foreach ($listCourses as $studentCourse) {
            if ($studentCourse->id === $uuidCourse) {
                return $course;
            }
        }

        $course->students()->save($student);

        return $course;
    }
}