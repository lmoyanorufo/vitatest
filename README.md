# Students && Courses  
  
  
For run this project is necessary to have the following :
  

 - PHP 7.1   
 - Composer
 - PHP extensions required by Laravel 5.6
 - MySQL 5.5 or higher

Apart of the above is necesary to have a database with this credentials:

 - DB_CONNECTION=mysql   
 - DB_HOST=127.0.0.1   
 - DB_PORT=3306  
 - DB_DATABASE=homestead   
 - DB_USERNAME=homestead   
 - DB_PASSWORD=homestead
 
## Installation

First of all install the dependencies with the following command:
```
composer install
```

It's necesary to have the required tables (student, courses...), those can
be created automatically by artisan migration mechanism.

```
php artisan migrate
```
 
To help with the operations some seeds has been included,
database can be populated with this command.

```
php artisan db:seed
```

For start the project:
```
php artisan serve
```

Open the browser with this url where the list of students should be displayed

```
http://127.0.0.1:8000/api/student
```

  
The list of enpoints can be checked on *routes/api.php*  

For post and put enpoints the payload must be provided
in JSON format. The response are also in JSON.


## Student endpoints  

| Method | Endpoint                                 | Description                              | Example Request Payload               |
|--------|------------------------------------------|------------------------------------------|---------------------------------------|
| [POST] | /api/student                             | Creates a new student                    | {"name":"Medicine","description":"-"} |
| [GET]  | /api/student                             | Retrieves all the students               |                                       |
| [GET]  | /api/student/{studentId}                  | Retrieves a student with the id provided |                                      |
| [PUT]  | /api/student/{studentId}                  | Edits a student                          | {"name":"Medicine","description":"-"}|
  
  
## Course endpoints  
  
| Method | Endpoint                                 | Description                             | Example Request Payload               |
|--------|------------------------------------------|-----------------------------------------|---------------------------------------|
| [POST] | /api/course                              | Creates a new course                    | {"name":"Medicine","description":"-"} |
| [GET]  | /api/course                              | Retrieves all the courses               |                                       |
| [GET]  | /api/course/{courseId}                   | Retrieves a course with the id provided |                                       |
| [PUT]  | /api/course/{courseId}                   | Edits a course                          | {"name":"Medicine","description":"-"} |
| [PUT]  | /course/{courseId}/student/{studentId}   | Add student to an existing course       | {}                                    |
  
  
Controllers has full code coverage. Can be checked on coverage/index.html

## TODO:   
 - Containerization + docker-compose
 - More coverage   
 - Implement Deleting endpoints   
 - Logging   
 - Behat
 - ...
