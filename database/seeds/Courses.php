<?php

use Illuminate\Database\Seeder;
use App\Models\Course;
use App\Models\Student;

class Courses extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 20; $i++) {

            /** @var Course $course */
            $course = Course::create([
                'name' => 'Medicine ' . $i,
                'description' => 'Very interesting'
            ]);

            if ($i == 0){
                $listStudents = Student::query()->get();
                $course->students()->saveMany($listStudents);
            }
        }
    }
}
