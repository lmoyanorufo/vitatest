<?php

use Illuminate\Database\Seeder;
use App\Models\Student;

class Students extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 20; $i++) {
            Student::create([
                'name' => 'Medicine ' . $i,
                'description' => 'Very interesting'
            ]);
        }
    }
}
