<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseStudents extends Migration
{
    const TABLE_NAME = 'course_student';

    public function up()
    {
        Schema::create(self::TABLE_NAME, function(Blueprint $table)
        {
            $table->string('student_id');
            $table->foreign('student_id')->references('id')
                ->on('students')->onDelete('cascade');

            $table->string('course_id');
            $table->foreign('course_id')->references('id')
                ->on('courses')->onDelete('cascade');

            $table->primary(['student_id', 'course_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE_NAME);
    }
}
