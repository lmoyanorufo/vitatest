<?php

namespace Tests\Unit\Http\Controllers;

use App\Http\Controllers\StudentController;
use App\Repository\StudentRepositoryInterface;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Validator;
use Symfony\Component\HttpFoundation\ParameterBag;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Tests\TestCase;

/**
 * Class StudentControllerTest
 * @package Tests\Unit\Http\Controllers
 *
 * This testcase has become a bit complicated (validation, views, etc)
 * So could be helpful to split in smaller pieces or delegate some functions
 * in another places as middlewares or any other tecnique.
 */
class StudentControllerTest extends TestCase
{
    /** @var StudentController */
    protected $sut;

    /** @var StudentRepositoryInterface| \PHPUnit_Framework_MockObject_MockObject */
    protected $studentRepositoryMock;

    /** @var ViewFactory| \PHPUnit_Framework_MockObject_MockObject */
    protected $viewFactoryMock;

    /** @var ValidationFactory| \PHPUnit_Framework_MockObject_MockObject */
    protected $validationFactoryMock;

    public function setUp()
    {
        $this->studentRepositoryMock = $this->createMock(StudentRepositoryInterface::class);
        $this->viewFactoryMock       = $this->createMock(ViewFactory::class);
        $this->validationFactoryMock = $this->createMock(ValidationFactory::class);
        $this->sut = new StudentController(
            $this->studentRepositoryMock,
            $this->viewFactoryMock,
            $this->validationFactoryMock
        );
    }

    /**
     * @test
     */
    public function itReturnsListOfStudents()
    {
        $expectedPayload = [1, 2, 3];
        $expectedContent = "hello";

        $this->viewFactoryMock
            ->expects(self::once())
            ->method('make')
            ->with(
                'api.default',
                [
                    'payload'  => $expectedPayload,
                    'metadata' => []
                ]
            )
            ->willReturn($expectedContent);

        $this->studentRepositoryMock
            ->expects(self::once())
            ->method('all')
            ->willReturn($expectedPayload);

        $resultResponse = $this->sut->index(new Response());

        self::assertEquals($resultResponse->getStatusCode(), 200);
        self::assertEquals($resultResponse->getContent(), $expectedContent);
    }

    /**
     * @test
     */
    public function itCreatesStudents()
    {
        $jsonPayload = new ParameterBag(['name' => "Hello", "description" => "Hey"]);
        $request = new Request();
        $request->setJson($jsonPayload);

        $mockValidator = $this->createMock(Validator::class);
        $mockValidator
            ->expects(self::once())
            ->method('validate');

        $this->validationFactoryMock
            ->expects(self::once())
            ->method('make')
            ->willReturn($mockValidator);

        $resultResponse = $this->sut->create($request, new Response());

        self::assertEquals($resultResponse->getStatusCode(), 200);
    }

    /**
     * @test
     */
    public function itDoesntCreateStudentsIfDontPassValidation()
    {
        $jsonPayload = new ParameterBag(["description" => "Hey"]);
        $request = new Request();
        $request->setJson($jsonPayload);

        $expectedErrorMessages = [
            'name' => "Name is missing"
        ];

        $mockValidator = $this->createMock(Validator::class);
        $validationException = new ValidationException($mockValidator);
        $mockValidator
            ->expects(self::once())
            ->method('validate')
            ->willThrowException($validationException);

        $mockValidator
            ->expects(self::once())
            ->method('errors')
            ->willReturn(new MessageBag($expectedErrorMessages));

        $this->validationFactoryMock
            ->expects(self::once())
            ->method('make')
            ->willReturn($mockValidator);


        $resultResponse = $this->sut->create($request, new Response());
        self::assertEquals($resultResponse->getStatusCode(), 400);
    }

    /** Update method are very similar to test */

    /**
     * @test
     */
    public function itUpdatesStudents()
    {
        $studentId        = "a";
        $jsonPayload = new ParameterBag(['name' => "Hello", "description" => "Hey"]);
        $request = new Request();
        $request->setJson($jsonPayload);

        $mockValidator = $this->createMock(Validator::class);
        $mockValidator
            ->expects(self::once())
            ->method('validate');

        $this->validationFactoryMock
            ->expects(self::once())
            ->method('make')
            ->willReturn($mockValidator);

        $resultResponse = $this->sut->update($request, new Response(), $studentId);

        self::assertEquals($resultResponse->getStatusCode(), 200);
    }

    /**
     * @test
     */
    public function itDoesntUpdateStudentsIfDontPassValidation()
    {
        $studentId        = "a";
        $jsonPayload = new ParameterBag(["description" => "Hey"]);
        $request = new Request();
        $request->setJson($jsonPayload);

        $expectedErrorMessages = [
            'name' => "Name is missing"
        ];

        $mockValidator = $this->createMock(Validator::class);
        $validationException = new ValidationException($mockValidator);
        $mockValidator
            ->expects(self::once())
            ->method('validate')
            ->willThrowException($validationException);

        $mockValidator
            ->expects(self::once())
            ->method('errors')
            ->willReturn(new MessageBag($expectedErrorMessages));

        $this->validationFactoryMock
            ->expects(self::once())
            ->method('make')
            ->willReturn($mockValidator);


        $resultResponse = $this->sut->update($request, new Response(), $studentId);
        self::assertEquals($resultResponse->getStatusCode(), 400);
    }

    /**
     * @test
     */
    public function itReturnsStudent()
    {
        $expectedPayload = [1];
        $expectedContent = "hello";
        $studentId       = "a";

        $this->viewFactoryMock
            ->expects(self::once())
            ->method('make')
            ->with(
                'api.default',
                [
                    'payload'  => $expectedPayload,
                    'metadata' => []
                ]
            )
            ->willReturn($expectedContent);

        $this->studentRepositoryMock
            ->expects(self::once())
            ->method('get')
            ->willReturn($expectedPayload);

        $resultResponse = $this->sut->show($studentId, new Response());

        self::assertEquals($resultResponse->getStatusCode(), 200);
        self::assertEquals($resultResponse->getContent(), $expectedContent);
    }
}