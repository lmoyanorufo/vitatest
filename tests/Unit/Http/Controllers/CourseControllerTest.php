<?php

namespace Tests\Unit\Http\Controllers;

use App\Http\Controllers\CourseController;
use App\Repository\CourseRepositoryInterface;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Validator;
use Symfony\Component\HttpFoundation\ParameterBag;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Tests\TestCase;


/**
 * Class CourseControllerTest
 * @package Tests\Unit\Http\Controllers
 *
 * This testcase has become a bit complicated (validation, views, etc)
 * So could be helpful to split in smaller pieces or delegate some functions
 * in another places as middlewares or any other tecnique.
 */
class CourseControllerTest extends TestCase
{
    /** @var CourseController */
    protected $sut;

    /** @var CourseRepositoryInterface| \PHPUnit_Framework_MockObject_MockObject */
    protected $courseRepositoryMock;

    /** @var ViewFactory| \PHPUnit_Framework_MockObject_MockObject */
    protected $viewFactoryMock;

    /** @var ValidationFactory| \PHPUnit_Framework_MockObject_MockObject */
    protected $validationFactoryMock;

    public function setUp()
    {
        $this->courseRepositoryMock  = $this->createMock(CourseRepositoryInterface::class);
        $this->viewFactoryMock       = $this->createMock(ViewFactory::class);
        $this->validationFactoryMock = $this->createMock(ValidationFactory::class);
        $this->sut = new CourseController(
            $this->courseRepositoryMock,
            $this->viewFactoryMock,
            $this->validationFactoryMock
        );
    }

    /**
     * @test
     */
    public function itReturnsListOfCourses()
    {
        $expectedPayload = [1, 2, 3];
        $expectedContent = "hello";

        $this->viewFactoryMock
            ->expects(self::once())
            ->method('make')
            ->with(
                'api.default',
                [
                    'payload'  => $expectedPayload,
                    'metadata' => []
                ]
            )
            ->willReturn($expectedContent);

        $this->courseRepositoryMock
            ->expects(self::once())
            ->method('all')
            ->willReturn($expectedPayload);

        $resultResponse = $this->sut->index(new Response());

        self::assertEquals($resultResponse->getStatusCode(), 200);
        self::assertEquals($resultResponse->getContent(), $expectedContent);
    }

    /**
     * @test
     */
    public function itAddsStudentToCourse()
    {
        $courseId  = "a";
        $studentId = "b";
        $studentCourses = ['a'];

        $courseInfo = new \stdClass();
        $courseInfo->id   = $courseId;
        $courseInfo->name = "Course";

        $expectedContent = json_encode($courseInfo);

        $this->courseRepositoryMock
            ->expects(self::once())
            ->method('listByStudent')
            ->with($studentId)
            ->willReturn($studentCourses);

        $this->courseRepositoryMock
            ->expects(self::once())
            ->method('addStudent')
            ->with($studentId, $courseId);

        $this->courseRepositoryMock
            ->expects(self::once())
            ->method('get')
            ->with($courseId)
            ->willReturn($courseInfo);

        $this->viewFactoryMock
            ->expects(self::once())
            ->method('make')
            ->with(
                'api.default',
                [
                    'payload'  => $courseInfo,
                    'metadata' => []
                ]
            )
            ->willReturn($expectedContent);


        $resultResponse = $this->sut->addStudent(new Response(), $courseId, $studentId);

        self::assertEquals($resultResponse->getStatusCode(), 200);
        self::assertEquals($resultResponse->getContent(), $expectedContent);
    }

    /**
     * @test
     */
    public function itReturnsErrorIfStudentHasMoreCoursesThanAllowed()
    {
        $courseId  = "a";
        $studentId = "b";
        $studentCourses = ['a', 'b'];
        $expectedContent = "Error";

        $this->courseRepositoryMock
            ->expects(self::once())
            ->method('listByStudent')
            ->with($studentId)
            ->willReturn($studentCourses);

        $this->courseRepositoryMock
            ->expects(self::never())
            ->method('addStudent');

        $this->courseRepositoryMock
            ->expects(self::never())
            ->method('get');

        $this->viewFactoryMock
            ->expects(self::once())
            ->method('make')
            ->with(
                'api.default',
                [
                    'payload'  => ['message' => "The student has reached the limit of courses"],
                    'metadata' => []
                ]
            )
            ->willReturn($expectedContent);


        $resultResponse = $this->sut->addStudent(new Response(), $courseId, $studentId);

        self::assertEquals($resultResponse->getStatusCode(), 403);
        self::assertEquals($resultResponse->getContent(), $expectedContent);
    }

    /**
     * @test
     */
    public function itCreatesCourses()
    {
        $jsonPayload = new ParameterBag(['name' => "Hello", "description" => "Hey"]);
        $request = new Request();
        $request->setJson($jsonPayload);

        $mockValidator = $this->createMock(Validator::class);
        $mockValidator
            ->expects(self::once())
            ->method('validate');

        $this->validationFactoryMock
            ->expects(self::once())
            ->method('make')
            ->willReturn($mockValidator);

        $resultResponse = $this->sut->create($request, new Response());

        self::assertEquals($resultResponse->getStatusCode(), 200);
    }

    /**
     * @test
     */
    public function itDoesntCreateCoursesIfDontPassValidation()
    {
        $jsonPayload = new ParameterBag(["description" => "Hey"]);
        $request = new Request();
        $request->setJson($jsonPayload);

        $expectedErrorMessages = [
            'name' => "Name is missing"
        ];

        $mockValidator = $this->createMock(Validator::class);
        $validationException = new ValidationException($mockValidator);
        $mockValidator
            ->expects(self::once())
            ->method('validate')
            ->willThrowException($validationException);

        $mockValidator
            ->expects(self::once())
            ->method('errors')
            ->willReturn(new MessageBag($expectedErrorMessages));

        $this->validationFactoryMock
            ->expects(self::once())
            ->method('make')
            ->willReturn($mockValidator);


        $resultResponse = $this->sut->create($request, new Response());
        self::assertEquals($resultResponse->getStatusCode(), 400);
    }

    /** Update method are very similar to test */

    /**
     * @test
     */
    public function itUpdatesCourses()
    {
        $courseId        = "a";
        $jsonPayload = new ParameterBag(['name' => "Hello", "description" => "Hey"]);
        $request = new Request();
        $request->setJson($jsonPayload);

        $mockValidator = $this->createMock(Validator::class);
        $mockValidator
            ->expects(self::once())
            ->method('validate');

        $this->validationFactoryMock
            ->expects(self::once())
            ->method('make')
            ->willReturn($mockValidator);

        $resultResponse = $this->sut->update($request, new Response(), $courseId);

        self::assertEquals($resultResponse->getStatusCode(), 200);
    }

    /**
     * @test
     */
    public function itDoesntUpdateCoursesIfDontPassValidation()
    {
        $courseId        = "a";
        $jsonPayload = new ParameterBag(["description" => "Hey"]);
        $request = new Request();
        $request->setJson($jsonPayload);

        $expectedErrorMessages = [
            'name' => "Name is missing"
        ];

        $mockValidator = $this->createMock(Validator::class);
        $validationException = new ValidationException($mockValidator);
        $mockValidator
            ->expects(self::once())
            ->method('validate')
            ->willThrowException($validationException);

        $mockValidator
            ->expects(self::once())
            ->method('errors')
            ->willReturn(new MessageBag($expectedErrorMessages));

        $this->validationFactoryMock
            ->expects(self::once())
            ->method('make')
            ->willReturn($mockValidator);


        $resultResponse = $this->sut->update($request, new Response(), $courseId);
        self::assertEquals($resultResponse->getStatusCode(), 400);
    }

    /**
     * @test
     */
    public function itReturnsCourse()
    {
        $expectedPayload = [1];
        $expectedContent = "hello";
        $courseId        = "a";

        $this->viewFactoryMock
            ->expects(self::once())
            ->method('make')
            ->with(
                'api.default',
                [
                    'payload'  => $expectedPayload,
                    'metadata' => []
                ]
            )
            ->willReturn($expectedContent);

        $this->courseRepositoryMock
            ->expects(self::once())
            ->method('get')
            ->willReturn($expectedPayload);

        $resultResponse = $this->sut->show($courseId, new Response());

        self::assertEquals($resultResponse->getStatusCode(), 200);
        self::assertEquals($resultResponse->getContent(), $expectedContent);
    }
}