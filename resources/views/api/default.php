<?php


$payload  = $payload ?? [];
$metadata = $metadata ?? [];

echo json_encode([
    'payload'  => $payload,
    'metadata' => $metadata
],
    JSON_PRETTY_PRINT
);