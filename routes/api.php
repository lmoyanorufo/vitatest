<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/** Students */
Route::get('/student/{id}', 'StudentController@show');
Route::get('/student', 'StudentController@index');
Route::post('/student', 'StudentController@create');
Route::put('/student/{id}', 'StudentController@update');

/** Courses */
Route::get('/course', 'CourseController@index');
Route::get('/course/{id}', 'CourseController@show');
Route::post('/course', 'CourseController@create');
Route::put('/course/{id}', 'CourseController@update');


/** Link student to course */
Route::put('/course/{courseId}/student/{studentId}', 'CourseController@addStudent');